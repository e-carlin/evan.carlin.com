# Personal website

This is my personal website

## Setup
This website is built using the static site generator
[pelican](https://github.com/getpelican/pelican). Pelican uses python. I use the
excellent tool [pyenv](https://github.com/pyenv/pyenv) to manage my python
versions. This project uses Python 3.7.4. Once python is installed creat a
virtualenv:
  - `cd evan.carlin.com`
  - `python -m venv venv`
  - `source venv/bin/activate`
  - `pip install .`

## Running the dev server
- `cd evan.carlin.com`
- `pelican content -s pelicanconf.py -v -D && pelican --listen -r --ignore-cache`
- Go to `localhost:8000`