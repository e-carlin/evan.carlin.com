Title: About Me

Welcome, I'm Evan Carlin. I'm many things but as it pertains to this site I'm
a programmer and an aspiring [hacker](http://www.paulgraham.com/hp.html) (not a
[cracker](https://www.cs.utah.edu/~elb/folklore/afs-paper/node9.html)).

I'm from Boulder, CO and currently live and work in Denver. Being from Boulder
I'm a fan of all of the "ings" (biking, skiing, running, ...). When not hacking
I'm almost certainly doing one of them.

For college I went to the University of Puget Sound (a choice based on tuition
and an "ing", skiing) where I majored in computer science. Before college the
closest I had come to programming was as a 12-year old I spent a weekend trying
to get a music tagging program running. At the time I had no clue that I was
programming. Not surprisingly I never got the software to build let alone do
what I needed. I took a CS class my freshmen year of college for a reason
approximating "interesting and I hopefully won't have to write a paper." I
stayed up most of the night working on the first assignment (moving a turtle
around the screen). It sounds corny to say but I was hooked from then on.

There are many reasons why I enjoy programming so much but below are a few that
stand out:

1. Abstraction. I have yet to experience any other profession that encourages
   abstracting away the details like software engineering. Any time I'm
   frustrated with some code I know my abstractions are not yet powerful enough
   and in time my frustration will turn to enjoyment.
2. Balance between the rote and the human. Code is an interface between
   programmers and other programmers, programmers and end-users and [incidentally
   programmers and computers](https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-7.html#%_chap_Temp_4)
   This balance makes every line of code interesting.
3. Working with thinkers. I find it stimulating and constantly rewarding
   to work with other people who are "thinkers." They are full of opinions,
   frequently funny, and always expanding my mind. Programming seems to have a
   fairly high concentration of these people. While I think there are downsides
   to the programming population and we desperately need more diversity I truly
   enjoy the community.

For a rundown of my work history past and present head over to [LinkedIn](https://www.linkedin.com/in/evan-carlin/)

For more info about this site such as why and how I created it please read this.

I'd love to hear from anyone that happened to find their way here. Please
send me an email at evan at carlin dot com.

Thanks for reading.
